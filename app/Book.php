<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Book extends Model
{
    protected $fillable = ['titulo','npaginas', 'edicion', 'autor', 'resumen', 'precio'];
}
