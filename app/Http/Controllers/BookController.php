<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Book;

class BookController extends Controller
{
    public function index()
    {
        $books = Book::orderBy('id', 'DESC')->paginate(5);
        return view('index', compact('books'));
    }

    public function create()
    {
        return view('add');
    }

    public function store(Request $request)
    {
        $this->validate($request, ['titulo' => 'required', 'npaginas' => 'required', 'edicion' => 'required', 'autor' => 'required', 'resumen' => 'required', 'precio' => 'required']);
        Book::create($request->all());
        return redirect()->route('index');
    }

    public function edit($id)
    {
        $book = Book::find($id);
        return view('edit', compact('book'));
    }

    public function update(Request $request, $id)
    {
        $this->validate($request, ['titulo' => 'required', 'npaginas' => 'required', 'edicion' => 'required', 'autor' => 'required', 'resumen' => 'required', 'precio' => 'required']);
        Book::find($id)->update($request->all());
        return redirect()->route('book.index');
    }

    public function destroy($id)
    {
        Book::find($id)->delete();
        return redirect()->route('index');
    }


}
