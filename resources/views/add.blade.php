@extends('layouts.layout')
@section('content')
    @if (count($errors) > 0)
        <div class="alert alert-danger">
            <strong>¡Lo sentimos!</strong><br>Revisa los campos obligatorios.<br><br>
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif
    <form method="post" action="{{route('store')}}" role="form">
        {{ csrf_field() }}
        <div class="form-group">
            <label for="exampleInputEmail1">Titulo del libro</label>
            <input type="text" class="form-control" name="titulo"
                   placeholder="Ingresa el nombre del libro">
        </div>
        <div class="form-group">
            <label for="exampleInputPassword1">Número de páginas</label>
            <input type="number" class="form-control" name="npaginas" placeholder="Páginas del libro">
        </div>
        <div class="form-group">
            <label for="exampleInputPassword1">Edicion</label>
            <input type="number" class="form-control" name="edicion" placeholder="Edición del libro">
        </div>
        <div class="form-group">
            <label for="exampleInputPassword1">Autor</label>
            <input type="text" class="form-control" name="autor" placeholder="Autor del libro">
        </div>
        <div class="form-group">
            <label for="exampleInputPassword1">Resumen</label>
            <input type="text" class="form-control" name="resumen" placeholder="Resumen del libro">
        </div>
        <div class="form-group">
            <label for="exampleInputPassword1">Precio</label>
            <input type="number" class="form-control" name="precio" placeholder="Precio del libro">
        </div>
        <button type="submit" href="{{ route('index') }}" class="btn btn-primary">Enviar</button>
    </form>
@endsection
