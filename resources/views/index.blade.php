@extends('layouts.layout')
@section('content')
    <table class="table">
        <h1 class="col-6 offset-4">Listado de libros</h1>
        <hr>
        <thead class="thead-dark">
        <tr>
            <th scope="col">Titulo</th>
            <th scope="col">NumPaginas</th>
            <th scope="col">Edicion</th>
            <th scope="col">Autor</th>
            <th scope="col">Resumen</th>
            <th scope="col">Precio</th>
            <th scope="col">Editar</th>
            <th scope="col">Eliminar</th>
        </tr>
        </thead>
        <tbody>
        @if($books->count())
            @foreach($books as $book)
                <tr>
                    <td>{{$book->titulo}}</td>
                    <td>{{$book->npaginas}}</td>
                    <td>{{$book->edicion}}</td>
                    <td>{{$book->autor}}</td>
                    <td>{{$book->resumen}}</td>
                    <td>${{$book->precio}}</td>
                    <td><a class="btn btn-primary btn-xs margen-boton"
                           href="{{ action('BookController@edit', $book->id) }}"><span
                                class="fas fa-pencil-alt"></span></a>
                    </td>
                    <td>
                        <form action="{{ action('BookController@destroy', $book->id )}}"
                              method="post">
                            {{csrf_field()}}
                            <input name="_method" type="hidden" value="DELETE">
                            <button class="btn btn-danger btn-xs margen-boton2" type="submit"><span
                                    class="fas fa-trash-alt"></span></button>
                        </form>
                    </td>
                </tr>
            @endforeach
        @else
            <tr>
                <td colspan="8" class="text-center"><p>Sin libros en el inventario</p></td>
            </tr>
        @endif
        </tbody>
    </table>
@endsection

